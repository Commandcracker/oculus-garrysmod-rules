$.getJSON('./config.json', function (data) {
    for(var j in data.rules) {
        for(var t in data.rules[j]) {
            $("#rules").append('<figure class="item"><div class="card"><div class="card-body" id="box-' + j + '"><h3 class="card-title">' + t + '</h3><hr></div></div></figure>');
            for(var a in data.rules[j][t]) {
                for(var f in data.rules[j][t][a]) {                    
                    $("#box-" + j).append("<" + f + ">" + data.rules[j][t][a][f] + "</" + f + ">");
                }
            }
        }
    }
});
